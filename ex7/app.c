#include <stdio.h>

#include <GL/glut.h>

void draw() {
	GLfloat qaBlack[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat qaGreen[] = { 0.0, 1.0, 0.0, 1.0 };
	GLfloat qaWhite[] = { 1.0, 1.0, 1.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_AMBIENT, qaGreen);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, qaGreen);
	glMaterialfv(GL_FRONT, GL_SPECULAR, qaWhite);
	glMaterialf(GL_FRONT, GL_SHININESS, 60.0);

	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0);
	const GLfloat kqDelta = .01;
	for(int i = -90; i < 90; ++i) {
		for(int j = -90; j < 90; ++j) {
			glVertex3f(j*kqDelta, i*kqDelta, -.2);
			glVertex3f((j + 1)*kqDelta, i*kqDelta, -.2);
			glVertex3f((j + 1)*kqDelta, (i + 1)*kqDelta, -.2);
			glVertex3f(j*kqDelta, (i+ 1)*kqDelta, -.2);
		}
	}

	glEnd();
	glFlush();
}

void init() {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	GLfloat qaAmbientLight[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat qaDiffuseLight[] = { 0.8, 0.8, 0.8, 1.0 };
	GLfloat qaSpecularLight[] = { 1.0, 1.0, 1.0, 1.0 };
	
	glLightfv(GL_LIGHT0, GL_AMBIENT, qaAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, qaDiffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, qaSpecularLight);

	GLfloat qaLightPosition[] = { .5, .5, 0.0, 1.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, qaLightPosition);

}
int main(int iArgc, char** cppArgv) {
	glutInit(&iArgc, cppArgv);
	glutInitDisplayMode(GLUT_SINGLE| GLUT_RGB);
	glutInitWindowSize(250, 250);
	glutInitWindowPosition(200, 200);
	glutCreateWindow("Isik Etkisi");
	init();
	glutDisplayFunc(draw);
	glutMainLoop();
	
	return 0;
}


