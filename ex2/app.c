#include <stdio.h>

#include <GL/glut.h>

void display(void){
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glPointSize(5.0f);
	
	glBegin(GL_POINTS);
        	glVertex2i(10,10);
		glVertex2i(10,210);
        	glVertex2i(210,210);
		glVertex2i(210,10);
	glEnd();
	
	glFlush();
}

int main(int argc, char *argv[]) {
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(500, 600);
	glutCreateWindow("OpenGL Merhaba Dunya");
	glClearColor(1.0, 1.0, 1.0, 1.0);
	gluOrtho2D(0.0, 220.0, 0.0, 100.0);
	glutDisplayFunc(display);
	glutMainLoop();
	
	return 0;
}
