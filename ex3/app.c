#include <stdio.h>
#include <math.h>

#include <GL/glut.h>

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(5.0f);
	
	glColor3f(0, 0, 1);
	glBegin(GL_POINTS);
		glVertex2f(0.5, 0);
	glEnd();

	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
		glVertex2f(-0.25, 0.25);
		glVertex2f(-0.75, 0.25);
	glEnd();

	glColor3f(0.5, 0.5, 0.5);
	glBegin(GL_POINTS);
	for(int i = 1; i < 360; i++) {
		float x = 0.25 * sin(((float)i) * 3.14 / 180);
		float y = 0.25 * cos(((float)i) * 3.14 / 180);
		glVertex2f(x, y);
	}
	glEnd();

	glColor3f(1, 0, 0);
	glBegin(GL_TRIANGLES);
		glVertex2f(0, 0.35);
		glVertex2f(0, 0.55);
		glVertex2f(0.75, 0.45);
	glEnd();

	glFlush();
}

int main(int argc, char *argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE| GLUT_RGBA);
	glutInitWindowPosition(500, 200);
	glutInitWindowSize(500, 600);
	glutCreateWindow("OpenGL Merhaba Dunya");
	glClearColor(1.0, 1.0, 1.0, 1.0);

	glutDisplayFunc(display);
	
	glutMainLoop();
	
	return 0;
}
